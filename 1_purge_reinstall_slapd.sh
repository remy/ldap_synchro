#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cwd=`pwd`
the_host=`hostname -s`
the_date=`date --rfc-3339=date`

if [ -f $DIR/.personnal_ldap ]; then
    . $DIR/.personnal_ldap;
else
    echo "Please enter your domain name (the realm, eg: my.domain.com) :"
    read REALM
    echo "Please enter your root cn for this domain (eg: cn=admin,dc=my,dc=domain,dc=com) :"
    read ROOTCN
    echo "Please enter the username to use for syncrepl (eg: cn=sync,dc=my,dc=domain,dc=com) :"
    read SYNCCN
    echo "Please enter the password to use for syncrepl (eg: secret) :"
    read SYNCPW
fi

DCREALM=`echo $REALM | awk -f $DIR/convert_realm.awk`
sed -i "s|my.domain.com|$REALM|g" $DIR/ldif_and_schemas/*/*.ldif
sed -i "s|cn=sync,dc=my,dc=domain,dc=com|$SYNCCN|g" $DIR/ldif_and_schemas/syncOps/olcSyncrepl_on_hdb.ldif
sed -i "s|secret|$SYNCPW|g" $DIR/ldif_and_schemas/syncOps/olcSyncrepl_on_hdb.ldif

echo "cloning schema2ldif. Then, you will be able to list your schemas with 'ldap-schema-manager -l'"
echo "And insert new ones with 'ldap-schema-manager -i myschema.schema'"
# https://github.com/fusiondirectory/schema2ldif
git clone https://github.com/fusiondirectory/schema2ldif.git
cp $DIR/schema2ldif/bin/* /usr/bin/
cp $DIR/schema2ldif/man/* /usr/share/man/man1/

# Need to backup config
# https://www.vincentliefooghe.net/content/sauvegarde-et-restauration-openldap
cd /etc
tar -zcvf etc-ldap.tar ldap
mv etc-ldap.tar $cwd
cd $cwd

sed -i "s|dc=my,domain,dc=com|$DCREALM|g" $DIR/cron.daily.ldapdumps

# backup of db is already done cron. 
echo 'The restoring part of this program in "2_setup_ldap_provider.sh" will try to restore a ldif file from '$DIR'/ldif_and_schemas/provider/db_to_restore.ldif'
echo "Do you need to backup your LDAP DB before erasing everything [Y/n] ? "
read BKP
if ! [ "$BKP" == "n" ];then
    cp $DIR/cron.daily.ldapdumps /etc/cron.daily/ldapdumps
    bash /etc/cron.daily/ldapdumps
    cp /var/backups/ldap_dumps/"$the_date"_dump.ldif.gz $DIR/ldif_and_schemas/provider/
    cd $DIR/ldif_and_schemas/provider/ && gunzip -d "$the_date"_dump.ldif.gz
    mv "$the_date"_dump.ldif db_to_restore.ldif
    cd $cwd
fi


# cleaning / purging
service slapd stop
apt-get -y remove slapd
apt-get -y purge slapd
rm -rf /var/lib/ldap
rm -rf /etc/ldap
rm -rf /var/backups/slapd*
rm -rf /var/backups/*ldapdb*

# reinstalling
apt-get -y install slapd ldapvi ldap-utils phpldapadmin
# here we will choose hdb backend + cn=admin + domain dc=my,dc=domain,dc=com
# realm my.domain.com

sed -i "s/ldap5/$the_host" $DIR/config_phpldapadmin.php
sed -i "s|cn=manager,dc=my,domain,dc=com|$ROOTCN|g" $DIR/config_phpldapadmin.php
sed -i "s|dc=my,domain,dc=com|$DCREALM|g" $DIR/config_phpldapadmin.php
sed -i "s|my.domain.com|$REALM|g" $DIR/config_phpldapadmin.php
cp $DIR/config_phpldapadmin.php /etc/phpldapadmin/config.php

sed -i "s|dc=my,domain,dc=com|$DCREALM|g" $DIR/2_setup_ldap_provider.sh
sed -i "s|cn=manager,dc=my,domain,dc=com|$ROOTCN|g" $DIR/ldif_and_schemas/provider/provider_mailing_list.ldif

echo "I will need to reconfigure your LDAP. Please select HDB as a backend..."

sleep 5

dpkg-reconfigure slapd
