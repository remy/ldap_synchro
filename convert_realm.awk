#!/usr/bin/env awk

BEGIN { 
    FS=".";
    ORS="";
}

{
    i=1 
    while ( i<=NF ) {
        if( i == NF ) {
            print "dc="$i;
            }
        else {
        print "dc="$i",";
        }
        i++;
    }
}

