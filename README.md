Ces scripts permettent de mettre en place deux serveurs OpenLDAP avec syncrepl de maniere rapide sur des distributions de type _debian-like_ (sans ldaps, avec juste quelques schemas (mais vous pouvez en rajouter dans le dossier `ldif_and_schemas/schemas/`). 

Ceci dit, ces scripts doivent pouvoir etre modifies assez facilement pour les rendre compatibles avec des distributions de type RedHat-like ou bien meme apres compilation depuis les sources.

Ces codes ne rajoutent pas l'utilisateur de synchro, c'est a vous a le faire avant si vous avez deja une base ou a rajouter le bon fichier ldif; dans le cas contraire, vous pouvez utiliser votre compte administrateur de domaine.

`slapd`, `ldapvi`, `ldap-utils` et `phpldapadmin` seront installes.
`ldapvi` et `phpldapadmin` sont optionnels, mais il faudra editer les scripts pour enlever toute reference a ces deux packages.

Dans le cas ou vous copiez ce dossier sur votre serveur, sachez que `git` est necessaire dans le premier script afin de cloner [schema2ldif](https://github.com/fusiondirectory/schema2ldif). Mais ce dernier est egalement facultatif.

**ATTENTION : le premier script efface tout et reinstalle tout de 0 !! Il propose toutefois de faire un backup avant de tout effacer, pour le restaurer ensuite. Vous pouvez aussi placer directement votre dump au format ldif dans un fichier nommé db_to_restore.ldif à placer dans ldif_and_schemas/provider/**


Clonez ce depot avec git sur la machine que vous souhaitez definir comme provider :

```bash
git clone http://remy@gitlab.mbb.univ-montp2.fr/remy/ldap_synchro.git
```

Ensuite, il faut lancer le premier script :

```bash
bash 1_purge_reinstall_slapd.sh
```

Lorsque vous avez fini de repondre au question et que vous avez reconfigure le serveur LDAP avec dpkg-reconfigure (pensez a choisir `HDB` comme _backend_),
vous pouvez lancer le script pour le provider :

```bash
bash 2_setup_ldap_provider.sh
```

Ce deuxieme script se termine en ouvrant `ldapvi`, ce qui vous permet de terminer la configuration de votre provider, notamment les valeurs **olcTimeLimit** et **olcSizeLimit** a mettre dans _'olcDatabase={-1}frontend,cn=config'_

Maintenant, vous vous connectez sur le slave, puis :

  - soit vous relancez le premier script ( `1_purge_reinstall_slapd.sh` ) apres avoir (encore) clone ce depot,
  - soit vous copiez le dossier `ldap_synchro` (ce dossier / ou se trouve le README.md) du provider vers l'esclave
    ex: 

    ```bash
    scp -r provider:/root/ldap_synchro .
    ```



Ensuite, il faut lancer cote esclave le dernier script :

```bash
bash 2_setup_ldap_slave.sh
```


Normalement, desormais, vous devez avoir deux serveurs LDAP en mode syncrepl.

Vous pouvez rajouter de la meme maniere un troisieme esclave LDAP, par contre, pensez a incrementer la valeur `rid=` dans `ldif_and_schemas/syncOps/olcSyncrepl_on_hdb.ldif`
