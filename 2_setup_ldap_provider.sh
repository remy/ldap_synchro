#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

the_host=`hostname -f`
sed -i "s|ldap2.my.domain.com|$the_host|" $DIR/ldif_and_schemas/syncOps/olcSyncrepl_on_hdb.ldif

# first; adding schemas
for schema in $DIR/ldif_and_schemas/schemas/*.ldif
    do
    /usr/bin/ldapadd -Q -vvv -Y EXTERNAL -H ldapi:/// -f $schema
done


# restoring database
service slapd stop
# Normally for a full dump the first two records are the domain itself + the LDAP manager, which are already set by dpkg-reconfigure, at the install step...
echo "I will remove the two first records from your LDIF backup file to avoid to try to insert it (the domain and the admin fields) in your new DB which normally have already been done at the install step..."
awk 'BEGIN{RS="# id=";} NR>3 {print "# id="$0}' $DIR/ldif_and_schemas/provider/db_to_restore.ldif > $DIR/ldif_and_schemas/provider/db_to_restore.tmp.ldif
/usr/sbin/slapadd -c -b dc=my,dc=domain,dc=com -l $DIR/ldif_and_schemas/provider/db_to_restore.tmp.ldif
mkdir /var/lib/ldap/accesslog
chown -R openldap:openldap /var/lib/ldap/

service slapd start
ldapadd -Q -Y EXTERNAL -H ldapi:/// -f $DIR/ldif_and_schemas/provider/provider_mailing_list_hdb.ldif

cp -p /var/lib/ldap/DB_CONFIG /var/lib/ldap/accesslog
service slapd restart

echo "Now, we need to modify 'olcSizeLimit' and 'olcTimeLimit' (seconds) in 'olcDatabase={-1}frontend,cn=config'"
echo "I will open config with ldapvi. Set it up to the desired value..."
sleep 5
# olcTimeLimit in seconds
# to
#olcSizeLimit: 3000
#olcTimeLimit: 36000
## it can be done easily with:
/usr/bin/ldapvi -h ldapi:// -Y EXTERNAL -b cn=config

