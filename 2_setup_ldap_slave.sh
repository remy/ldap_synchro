#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
backend="hdb"

# backend must be HDB. http://www.openldap.org/its/index.cgi?findid=8672

echo 'Schema will be added using ldif format. If you have only *.schema,
please use ldap-schema-manager -i your-schema.schema'

sleep 2

for schema in $DIR/ldif_and_schemas/schemas/*.ldif
do
    /usr/bin/ldapadd -vvv -Q -Y EXTERNAL -H ldapi:/// -f $schema
done

/usr/bin/ldapadd -vvv -Q -Y EXTERNAL -H ldapi:/// -f $DIR/ldif_and_schemas/modules/add_syncprov_module.ldif
/usr/bin/ldapadd -Q -vvv -Y EXTERNAL -H ldapi:/// -f $DIR'/ldif_and_schemas/syncOps/indices_on_'$backend'.ldif'
/usr/bin/ldapadd -Q -vvv -Y EXTERNAL -H ldapi:/// -f $DIR'/ldif_and_schemas/syncOps/olcSyncrepl_on_'$backend'.ldif'
/usr/bin/ldapadd -Q -vvv -Y EXTERNAL -H ldapi:/// -f $DIR'/ldif_and_schemas/syncOps/add_olc_updateref_on_'$backend'.ldif'

service slapd restart
